#ifndef MMAZE_TUILE_HPP
#define MMAZE_TUILE_HPP

#include "case.hpp"
#include "mur.hpp"
#include "site.hpp"
#include "melangeur.hpp"
#include "Graphe.hpp"

#include <vector>
#include <map>
#include <set>
#include <iostream>

namespace MMaze {

const int MAX_CASE = 16 ;   // nombre de cases sur une tuile
const int MAX_MUR = 24 ;    // nombre de murs sur une tuile

// Décrit une tuile par son index dans le Plateau
// et le numéro de sa case
struct TuileCase {
  int index_tuile ;
  int index_case ;

  friend bool operator==(const TuileCase& t1, const TuileCase& t2) {
    return t1.index_tuile == t2.index_tuile && t1.index_case == t2.index_case ;
  }
};

// Type d'une tuile
enum TuileType {
  TUILE_DEFAULT,
  TUILE_DEPART,
  TUILE_OBJECTIF,
  TUILE_SORTIE
};

/**
 * Tuile 4x4
 * @param type    Type de la tuile (spéciale ou non)
 * @param couleur Couleur de la tuile (si objectif/sortie)
 */
class Tuile {
  public :
    Tuile(TuileType type = TUILE_DEFAULT, Couleur couleur = AUCUNE) ;

    ~Tuile() ;

    //indique si deux cases voisines sont separees par un mur
    bool mur(Mur m) const ;

    //indique si une case est accessible depuis les portes ou non
    bool accessible(Case c) const ;

    //affichage
    friend std::ostream& operator<<(std::ostream& out, const Tuile& t) ;

    // Lecture/écriture de tuile dans des fichiers
    friend void write_tuile(const Tuile& tuile, const char* path) ;
    friend Tuile read_tuile(const char* path) ;

    // applique une rotation sur la tuile
    Tuile tourne(int rotation) const ;

    // Retourne une porte aléatoirement parmis les portes dispo
    int porte_aleatoire() ;

	   Graphe<TuileCase> tuileToGraphe(int idTuile) ;

  private :
    // On utilise des tableaux (plutôt que conteneurs STL)
    // pour pouvoir utiliser le mélangeur générique (généricité du C)

    // Ensemble des murs de la tuile (accessible par index)
    // vrai si mur faux sinon
    bool murs[MAX_MUR] ;

    // Ensemble des sites de la tuile, accessibles par leur indice de case
    //typedef std::map<int, Site> map_sites_t;
    //map_sites_t sites;
    // !!! std::map ne fonctionne pas avec melangeur.hpp
    // On peut au maximum avoir 8 sites sur une tuile : 4 portes et 4 departs
    // Sinon c'est 4 portes et 1 objectif/sortie
    Site sites[8] ;
    int nb_sites ;

    // Ensemble des boutiques (cases non accessible) par index
    // vrai si boutique faux sinon
    bool boutiques[MAX_CASE] ;

    // indique si une case est un site
    bool site(int c) const ;
    Site getSite(int c) const ;

    //affichage
    void afficher_horizontal(std::ostream& out, unsigned int i) const ;
    void afficher_vertical(std::ostream& out, unsigned int i) const ;

    void afficher_porte(std::ostream& out, unsigned int pos) const ;
    void afficher_case(std::ostream& out, unsigned int pos) const ;

    // Créer 1 à 4 portes aléatoirement
    void creer_portes() ;

    // Remplit tout les murs et les détruit aléatoirement jusqu'à rendre toutes
    // les cases accessibles (avec Union-Find)
    void creer_murs() ;

    // Créer les sites aléatoirement selon les règles suivantes
    // - pas sur un autre site (porte compris)
    // - max 1 sortie ou 1 objectif
    void creer_sites() ;

    // Créer un site à partir des infos fournies en paramètres
    void creer_site(int position, SiteType type, Couleur couleur) ;

    void creer_site_unique(SiteType type, Couleur couleur) ;

    void tuile_depart() ;

    // Vide la tuile de ses informations :
    // tout les murs à false
    // aucun sites
    void clear() ;

    /* GESTION BOUTIQUES */
    bool mur_direction(int index_case, Direction dir) ;
    bool impasse(int index_case) ;
    void nettoyer_boutiques() ;

} ;

Melangeur creer_melangeur_couleurs() ;

// retourne un entier aléatoire entre min et max compris
int rand_borne(int min, int max) ;

// Retourne la rotation à appliquer à une tuile droite (porte 13 vers le bas)
// selon la porte pour y accèder :
// ex : on sort par la 4 -> on rentre par la 11 -> tourne d'un cran
int rotation_porte_acces(int porte_index) ;

// Retourne la porte d'entrée associé à une porte de sortie
// ex : on sort par 4 -> on rentre par 11
int porte_entree(int porte_sortie) ;

} //end of namespace MMaze

#endif
