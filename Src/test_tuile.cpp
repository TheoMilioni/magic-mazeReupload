#include "tuile.hpp"
#include "melangeur.hpp"
#include "couleurs.hpp"
#include "pioche.hpp"
#include "Plateau.hpp"

#include <iostream>
#include <cstdlib>
#include <cassert>

using namespace MMaze ;

void section(std::string titre) {
  std::cout << titre << std::endl ;
  for (unsigned int i = 0; i < titre.length(); i++) {
    std::cout << "=" ;
  }
  std::cout << std::endl ;
}

void test_tuiles() {
  section("Test tuiles:") ;

  std::cout << "Tuile lambda :\n" ;
  Tuile t ;
  std::cout << t << "\n\n" ;

  std::cout << "Tuile départ :\n" ;
  Tuile tDep(TUILE_DEPART) ;
  std::cout << tDep << "\n\n" ;
  for (int i : {2, 4, 11, 13, 5, 6, 9, 10}) {
    assert(tDep.accessible(Case(i))) ;
  }

  std::cout << "Tuile objectif vert :\n" ;
  Tuile tObj(TUILE_OBJECTIF, VERT) ;
  std::cout << tObj << "\n\n" ;
}

void test_tuile() {
  Tuile t ;
  std::cout << t << std::endl ;
}

void test_couleur() {
  std::cout
    << TXT_JAUNE << JAUNE
    << TXT_VERT << VERT
    << TXT_ORANGE << ORANGE
    << TXT_VIOLET << VIOLET
    << TXT_CLEAR
    << BG_JAUNE << JAUNE
    << BG_VERT << VERT
    << BG_ORANGE << ORANGE
    << BG_VIOLET << VIOLET
    << TXT_CLEAR
    << std::endl ;
}

void test_pioche() {
  Pioche pioche(10) ;
  assert(pioche.taille() == 10) ;

  Tuile t = pioche.piocher() ;
  assert(pioche.taille() == 9) ;

  std::cout << "Tuile piochee :\n" << t << std::endl ;
}

void test_tuile_fichiers() {
  Tuile t ;
  std::cout << t << std::endl ;
  const char* path_out = "../tuiles/tuile_test/txt" ;
  write_tuile(t, path_out) ;

  //const char* path_in = "../tuiles/tuile_verif_lect.txt" ;
  //read_tuile(path_in) ;
}

void test_rotation() {
  Tuile t ;
  Tuile t2 ;
  std::cout << t << std::endl ;
  t2 = t.tourne(2) ;
  std::cout << t2 << std::endl ;
}
void test_graphe() {
	Tuile t ;
	Graphe<TuileCase> g ;
	std::cout << t << std::endl ;
	g = t.tuileToGraphe(1) ;

	std::vector<sommet> voisins4 = g.voisins(2) ;
	std::cout << "Voisins de 2  : " ;
	for (sommet s : voisins4) {
		std::cout << s << " " ;
	}
		std::cout<<std::endl ;

	std::vector<sommet> voisins10 = g.voisins(3) ;
	std::cout << "Voisins de 3 : " ;
	for (sommet s : voisins10) {
		std::cout << s << " " ;
	}
		std::cout<<std::endl ;

}

void test_plateau() {
  Plateau p ;
  //std::cout << p ;
  p.passer_porte({0, 2}) ;
  //std::cout << p ;
  p.passer_porte({0, 4}) ;
  std::cout << "Plateau (3 tuiles)" << std::endl << p ;
  std::vector<TuileCase> chemin = p.chemin({0, 5}, {2, 4}) ;
  std::cout << "Regarder 1ere et 3eme tuile (respectivement 0 et 2)" << std::endl ;
  std::cout << "Case notée : {n° tuile, n° case}" << std::endl ;
  std::cout << "Chemin de {0, 5} à {2, 4} (porte 4) : " ;
  for (TuileCase tc : chemin) {
    std::cout << "{" << tc.index_tuile << ", " << tc.index_case << "} " ;
  }
  std::cout << std::endl ;

  std::cout << "Chemin de {0, 5} à {0, 4} : " ;
  std::vector<TuileCase> chemin1 = p.chemin({0, 5}, {0, 4}) ;
  for (TuileCase tc : chemin1) {
    std::cout << "{" << tc.index_tuile << ", " << tc.index_case << "} " ;
  }
  std::cout << std::endl ;

  std::cout << "Chemin de {2, 11} à {2, 4} : " ;
  std::vector<TuileCase> chemin2 = p.chemin({2, 11}, {2, 4}) ;
  for (TuileCase tc : chemin2) {
    std::cout << "{" << tc.index_tuile << ", " << tc.index_case << "} " ;
  }
  std::cout << std::endl ;
}


int main() {
  srand(time(NULL)) ;

  //test_tuile() ;
  //test_tuiles() ;
  //test_pioche() ;
  //test_couleur() ;
  //test_rotation() ;
  test_graphe() ;
  //test_tuile_fichiers() ;
  test_plateau() ;

  return 0 ;
}
