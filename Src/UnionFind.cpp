#include "UnionFind.hpp"

#include <iostream>
#include <cassert>

Node::Node(int _valeur) {
  valeur = _valeur ;
  hauteur_approx = 1 ;
  // Par défaut noeud est son propre représentant
  parent = this ;
}

UnionFind::UnionFind(int size) {
  for (int i = 0; i < size; i++) {
    nodes.push_back(new Node(i)) ;
    classes_done.push_back(false) ;
  }
}

UnionFind::~UnionFind() {
  for (Node* node : nodes) {
    delete node ;
  }
}

Node* UnionFind::Find(Node* n) {
  assert(n) ;

  if (n->parent != n) {
    n->parent = Find(n->parent) ;
  }
  return n->parent ;
}

void UnionFind::Union(Node *n1, Node *n2) {
  Node* repr1 = Find(n1) ;
  Node* repr2 = Find(n2) ;

  if (repr1->hauteur_approx > repr2->hauteur_approx) {
    repr2->parent = repr1 ;
    classes_done[repr2->valeur] = true ;
  }
  else if (repr1->hauteur_approx < repr2->hauteur_approx) {
    repr1->parent = repr2 ;
    classes_done[repr1->valeur] = true ;
  }
  else { // Noeuds de même hauteur
    repr1->parent = repr2 ;
    repr2->hauteur_approx++ ;
    classes_done[repr1->valeur] = true ;
  }
}

bool UnionFind::done() {
  bool ok = true ;
  unsigned int i = 0 ;
  while (ok && i < classes_done.size()) {
    ok = ok && classes_done[i] ;
    i++ ;
  }
  return  ok ;
}

Node* UnionFind::node(int n) {
  return nodes[n] ;
}
