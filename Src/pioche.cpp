#include "pioche.hpp"
#include "tuile.hpp"
#include "couleurs.hpp"

#include <cassert>

namespace MMaze {

Pioche::Pioche(unsigned int taille)
  : tuiles(sizeof(Tuile)) {

  // Au moins depart + objectifs / sorties
  //assert(taille >= 1 + 4 * 2) ;

  // /!\ C'est le plateau qui génére la tuile de départ
  // 1 tuile de départ
  //Tuile tDepart(TUILE_DEPART) ;
  //tuiles.inserer(&tDepart) ;

  // 4 tuiles objectif / sortie
  for (Couleur couleur : {JAUNE, VERT, ORANGE, VIOLET}) {
    Tuile tObjectif(TUILE_OBJECTIF, couleur) ;
    Tuile tSortie(TUILE_SORTIE, couleur) ;
    tuiles.inserer(&tObjectif) ;
    tuiles.inserer(&tSortie) ;
  }

  for (unsigned int i = 4 * 2; i < taille; i++) {
    Tuile t ;
    tuiles.inserer(&t) ;
  }

  assert(tuiles.taille() == (int) taille) ;
}

// Mélangeur libère déjà la mémoire
// !!! conteneur dinamyque STL ne fonctionne pas avec melangeur
Pioche::~Pioche() {}

Tuile Pioche::piocher() {
  Tuile tuile;
  tuiles.retirer(&tuile) ;

  return tuile ;
}

int Pioche::taille() {
  return tuiles.taille() ;
}

}
