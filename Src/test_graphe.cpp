#include "Graphe.hpp"
#include <iostream>
#include <cassert>

using namespace MMaze ;

void verif_chemin_0_3(std::vector<sommet> chemin, const char* algo) {
  assert(chemin.size() == 3) ;
  assert(chemin[0] == 0) ;
  assert(chemin[1] == 1) ;
  assert(chemin[2] == 3) ;
  std::cout << "Chemin de 0 à 3 (" << algo << "): " ;
  for (sommet s : chemin) {
    std::cout << s << " " ;
  }
  std::cout << std::endl ;
}

int main() {
  Graphe<int> g ;

  for (int i = 0; i < 4; i++) g.add_sommet(i) ;
  assert(g.sommets() == 4) ;

  g.add_arc(0, 1) ;
  g.add_arc(0, 2) ;
  g.add_arc(1, 2) ;
  g.add_arc(1, 3) ;

  std::vector<sommet> voisins = g.voisins(1) ;
  std::cout << "Voisins de 1: " ;
  for (sommet s : voisins) {
    std::cout << s << " " ;
  }
  std::cout << std::endl ;

  // Test BFS (parcours en largeur)
  std::vector<int> dist ;
  std::vector<sommet> prec ;
  bfs(g, 0, dist, prec) ;

  assert(dist[3] == 2) ;
  assert(prec[1] == 0) ;
  assert(prec[3] == 1) ;

  // Test chemin + court avec BFS
  std::vector<sommet> chemin_bfs = chemin_plus_court_bfs(g, 0, 3) ;
  verif_chemin_0_3(chemin_bfs, "BFS") ;

  std::vector<sommet> chemin_astar = chemin_plus_court_a_star(g, 0, 3) ;
  verif_chemin_0_3(chemin_astar, "A*") ;

  // Test fusion
  Graphe<int> g2 ;
  g2.add_sommet(1000) ;
  g2.add_sommet(1001) ;
  g2.add_sommet(1002) ;
  g2.add_arc(0, 2) ;
  g2.add_arc(1, 2) ;

  g.fusion(g2) ;
  //g2.print_liste_adj() ; std::cout << "\n\n" ;
  //g.print_liste_adj() ;

  return 0 ;
}
