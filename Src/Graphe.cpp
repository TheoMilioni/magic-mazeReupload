#include "Graphe.hpp"
#include <cassert>
#include <iostream>

#include <queue>
#include <stack>
#include <limits>

#include "Plateau.hpp"

namespace MMaze {

/* GRAPHE */

template <typename T>
Graphe<T>::Graphe() : m_sommets(0) {}

template <typename T>
Graphe<T>::~Graphe() {}

template <typename T>
unsigned int Graphe<T>::sommets() const {
  return m_sommets ;
}

template <typename T>
sommet Graphe<T>::add_sommet(T element) {
  // Stocke la donnée
  data.push_back(element) ;
  // Ajoute un sommet au graphe (même indice que le vctor de data)
  arcs.push_back(std::vector<sommet>()) ;
  m_sommets++ ;
  return m_sommets ;
}

template <typename T>
sommet Graphe<T>::get_sommet(T element) const {
  unsigned int index = 0 ;
  bool trouve = false ;
  // Parcours le vector pour trouver l'élément
  while (index < data.size() && !trouve) {
    if (data[index] == element) {
      trouve = true ;
    }
    else {
      index++ ;
    }
  }
  // Retourne le nb de sommets si l'élement n'existe pas
  // on ne peut pas -1 car sommet = unsigned int
  return (trouve ? index : m_sommets) ;
}

template <typename T>
T Graphe<T>::get_element(sommet s) const {
  assert (s < m_sommets) ;
  return data[s] ;
}

template <typename T>
void Graphe<T>::add_arc(sommet s1, sommet s2) {
  assert(s1 < m_sommets) ;
  assert(s2 < m_sommets) ;

  // Double lien : graphe non orienté
  arcs[s1].push_back(s2) ;
  arcs[s2].push_back(s1) ;
}

template <typename T>
void Graphe<T>::add_arc_long() {
	for(sommet s = 0 ; s < 14 ; s++) {

		// on prend les 2 premieres colonnes de la tuile
		//voisins de droite
		if (s%4 == 0)
			arc_long_3(s,1) ;
		if (s%4== 1)
			arc_long_2(s,1) ;

		//on prend les 2 premières lignes de la tuile
		//voisins du bas
		if(s<4)
			arc_long_3(s,4) ;
		if(s>3 && s<8)
			arc_long_2(s,4) ;
	}
}

template <typename T>
void Graphe<T>::arc_long_3(sommet s, int i) {
	for(sommet s1 : voisins(s)) {
		if (s+i == s1){
			for (sommet s2 : voisins(s+2*i)) {
				if(s1 == s2) {
					add_arc(s,s+2*i) ;
					for(sommet s3 : voisins(s+3*i)) {
						if(s + 2*i == s3)
							add_arc(s,s+3*i) ;
					}
				}
			}
		}
	}
}

template <typename T>
void Graphe<T>::arc_long_2(sommet s, int i) {
	for(sommet s1 : voisins(s)) {
		if (s+i == s1) {
			for (sommet s2 : voisins(s+2*i)) {
				if(s1 == s2)
					add_arc(s,s+2*i) ;
			}
		}
	}
}

template <typename T>
std::vector<sommet> Graphe<T>::voisins(sommet s) const {
  assert(s < m_sommets) ;
  return arcs[s] ;
}

template <typename T>
void Graphe<T>::fusion(Graphe& g) {
  // offset : décalage à applique à valeur de la case pour son nouveau n° de sommet
  int offset = m_sommets ;
  // Ajoute chaque sommet de g au graphe
  // nouveau sommet : g + offset
  for (sommet s = 0; s < g.sommets(); s++) {
    T element = g.data[s] ;
    add_sommet(element) ;

    // Récupère les anciens voisins et applique l'offset pour ajouter l'arc
    std::vector<sommet> ex_voisins = g.arcs[s] ;
    for (sommet v : ex_voisins) {
      arcs[s + offset].push_back(v + offset) ;
    }
  }
}

template <typename T>
std::vector<T> Graphe<T>::get_elements_chemin(std::vector<sommet> chemin) {
  std::vector<T> elements ;
  for (sommet s : chemin) {
    elements.push_back(get_element(s)) ;
  }
  return elements ;
}

// Affiche les listes d'adjacences :
template <typename T>
void Graphe<T>::print_liste_adj() {
  for (sommet s = 0; s < m_sommets; s++) {
    std::cout << s << ": " ;
    for (sommet v : arcs[s]) {
      std::cout << v << " " ;
    }
    std::cout << std::endl ;
  }
}

/** Recherche de chemin le plus court dans graphe */

std::vector<sommet> chemin(const std::vector<sommet>& prec, sommet start, sommet end) {
  std::stack<sommet> pile ;
  std::vector<sommet> chemin ;

  // Remplit la pile avec tout les prédecesseurs sauf start/end
  sommet predecesseur = prec[end] ;
  while (predecesseur != start) {
    pile.push(predecesseur) ;
    predecesseur = prec[predecesseur] ;
  }

  // Transfère la pile dans le vector
  chemin.push_back(start) ;
  while (!pile.empty()) {
    chemin.push_back(pile.top()) ;
    pile.pop() ;
  }
  chemin.push_back(end) ;

  return chemin ;
}

template <typename T>
void bfs(const Graphe<T>& g,
         sommet start,
         std::vector<int>& dist,
         std::vector<sommet>& prec)
{
  std::queue<sommet> file ;

  // Initialise les distances à +infini
  // Pour faire office de marquage
  dist.clear() ;
  for (unsigned int i = 0; i < g.sommets(); i++) {
    dist.push_back(std::numeric_limits<int>::max()) ;
  }

  // Initialise les prédecesseurs
  prec.clear() ;
  prec.reserve(g.sommets()) ;

  dist[start] = 0 ;
  file.push(start) ;

  while (!file.empty()) {
    sommet s = file.front() ;
    file.pop() ; // pop en 2 temps : accès (front) puis suppr (pop)

    for (sommet voisin : g.voisins(s)) {
      if (dist[voisin] == std::numeric_limits<int>::max()) {
        dist[voisin] = dist[s] + 1 ;
        prec[voisin] = s ;
        file.push(voisin) ;
      }
    }
  }
}

template <typename T>
std::vector<sommet> chemin_plus_court_bfs(const Graphe<T>& g, sommet start, sommet end) {
  std::vector<int> dist ;
  std::vector<sommet> prec ;
  std::queue<sommet> file ;
  bool trouve = false ;

  // Algo quasiment identique au précedent : cf commentaire prec. pour la 1ere
  // partie

  dist.clear() ;
  for (unsigned int i = 0; i < g.sommets(); i++) {
    dist.push_back(std::numeric_limits<int>::max()) ;
  }

  // Initialise les prédecesseurs
  prec.clear() ;
  prec.reserve(g.sommets()) ;

  dist[start] = 0 ;
  file.push(start) ;

  while (!file.empty() and !trouve) {
    sommet s = file.front() ;
    file.pop() ; // pop en 2 temps : accès (front) puis suppr (pop)

    for (sommet voisin : g.voisins(s)) {
      if (dist[voisin] == std::numeric_limits<int>::max()) {
        dist[voisin] = dist[s] + 1 ;
        prec[voisin] = s ;
        file.push(voisin) ;
      }
      if (voisin == end) {
        trouve = true ;
      }
    }
  }

  return trouve ? chemin(prec, start, end) : std::vector<sommet>() ;
}

// Donnée pour la file à priorité
struct AStarData {
  sommet index ;
  int priorite ;

  // Permet comparaison (pour priority_queue)
  bool operator< (const AStarData& d) const {
    return d.priorite < priorite ;
  }
};

template <typename T>
int heuristique(const Graphe<T>& g, sommet start, sommet end) {
  return 1 ; // Pas nécessaire on utilise que Graphe<TuileCase> et Graphe<int>
}

int heuristique(const Graphe<TuileCase>& g, sommet start, sommet end) {
  const TuileCase t_start = g.get_element(start) ;
  const TuileCase t_end = g.get_element(end) ;
  int i_tuile1 = t_start.index_tuile ;
  int i_tuile2 = t_end.index_tuile ;
  if (i_tuile1 != i_tuile2) {
    // Si tuile différente : retourne la différence entre les tuiles
    // -> minorant
    return (i_tuile1 > i_tuile2) ? i_tuile1 - i_tuile2 : i_tuile2 - i_tuile1 ;
  }
  else {
    return longueur_noeuds( t_start.index_case, t_end.index_case) ;
  }
}

int heuristique(const Graphe<int>& g, sommet start, sommet end) {
  return (end > start) ? end - start : start - end ;
}

template <typename T>
std::vector<sommet> chemin_plus_court_a_star(const Graphe<T>& g, sommet start, sommet end) {
  std::vector<int> dist ;       // Distances par rapport à start
  std::vector<sommet> prec ;    // Prédecesseurs
  std::priority_queue<AStarData> file ;

  // init toutes les distances à +inf
  dist.clear() ;
  for (unsigned int i = 0; i < g.sommets(); i++) {
    dist.push_back(std::numeric_limits<int>::max()) ;
  }

  // Réserve la place pour les prédecesseurs
  prec.clear() ;
  prec.reserve(g.sommets()) ;

  dist[start] = 0 ;
  prec[start] = start ;
  file.push({start, 0}) ;

  while (!file.empty()) {
    // s = file.pop() en 3 temps
    AStarData data = file.top() ;
    sommet s = data.index ;
    file.pop() ;

    for (sommet v : g.voisins(s)) {
      int dist_v = dist[s] + 1 ;
      if (dist_v < dist[v]) {
        dist[v] = dist_v ;
        prec[v] = s ;
        // Remets v dans la file avec nouvelle prio
        file.push({v, dist[v] + heuristique(g, v, end)}) ;
      }
    }
  }

  // si end à été atteint depuis start
  if (dist[end] != std::numeric_limits<int>::max()) {
    return chemin(prec, start, end) ;
  }
  else {
    return std::vector<sommet>() ;
  }
}

// Force la compilation des valeurs que T peut prendre
// sinon -> mettre code dans header pour T générique ?

template class Graphe<int> ;
template class Graphe<TuileCase> ;
template void bfs(const Graphe<int>& g, sommet start, std::vector<int>& dist, std::vector<sommet>& prec) ;
template void bfs(const Graphe<TuileCase>& g, sommet start, std::vector<int>& dist, std::vector<sommet>& prec) ;
template std::vector<sommet> chemin_plus_court_bfs(const Graphe<int>& g, sommet start, sommet end) ;
template std::vector<sommet> chemin_plus_court_bfs(const Graphe<TuileCase>& g, sommet start, sommet end) ;
template std::vector<sommet> chemin_plus_court_a_star(const Graphe<TuileCase>& g, sommet start, sommet end) ;
template std::vector<sommet> chemin_plus_court_a_star(const Graphe<int>& g, sommet start, sommet end) ;

}
