#ifndef MMAZE_MELANGEUR_HPP
#define MMAZE_MELANGEUR_HPP

#include <array>

namespace MMaze {

/**
 * Mélangeur générique
 * @param octets taille des objets stockés
 */
class Melangeur {
  public :
    Melangeur(int octets) ;

    ~Melangeur() ;

    /**
     * Insère une élément depuis une adresse
     * @param elem addresse générique (void*) de l'élement à insérer
     */
    void inserer(const void* elem) ;

    /**
     * Insère n éléments
     * @param elems adresse du début du tableau d'éléments
     * @param n     nombre d'éléments (= taille du tableau elems)
     */
    void inserer(const void* elems, size_t n) ;

    /**
     * Retire un élément et le copie à l'adresse fournie
     * @param elem adresse où copier l'élément retiré
     */
    void retirer(void* elem) ;

    // Vide le mélangeur
    void vider() ;

    // Retourne la taille du mélangeur
    int taille() ;

  private :
    char* mData ;     // Données
    int mTaille ;     // Nombre d'élements
    int mCapacite ;   // Nombre max d'éléments
    int mOctets ;     // Taille en octets d'un élément

    // Retourne un indice random de {0, taille}
    int indice_random() ;

    // Double la capacite du mélangeur (capacite * 2)
    void agrandit_capacite() ;

} ;

} //end of namespace MMaze

#endif
