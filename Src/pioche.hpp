#ifndef MMAZE_PIOCHE_HPP
#define MMAZE_PIOCHE_HPP

#include "melangeur.hpp"
#include "tuile.hpp"

namespace MMaze {

/**
 * Pioche de Tuile aléatoire
 * @param taille taille de la pioche (en nombre de tuile)
 */
class Pioche {
  public:
    /**
     * Constructeur Pioche
     * @param taille nombre de tuile à créer
     *
     * La taille doit être supérieure ou égale à 9 pour que le jeu soit
     * fonctionnel.
     */
    Pioche(unsigned int taille = 20) ;

    // libère la mémoire utilisée
    ~Pioche() ;

    // Retourne une tuile aléatoire
    Tuile piocher() ;

    // Nombre de tuiles dans la pioche
    int taille() ;

  private:
    // Melangeur interne où sont stockées les tuiles
    Melangeur tuiles ;
};

}

#endif // MMAZE_GENERATEUR_TUILE_HPP
