#ifndef MMAZE_UNION_FIND_HPP
#define MMAZE_UNION_FIND_HPP

#include <vector>

/**
 * Noeud dans un arbre Union-Find
 * @param valeur valeur entière du noeud
 */
class Node {
public:
  Node(int valeur) ;

  int valeur ;          // valeur du noeud
  int hauteur_approx ;  // hauteur approximative (heuristique pour compression)
  Node* parent ;        // noeud parent
} ;

/**
 * Union-Find fourni opérations Union(n1, n2) et Find(n)
 * @param size nombre de sets de départ
 */
class UnionFind {
  public:
    UnionFind(int size) ;
    ~UnionFind() ;

    /**
     * Retourne le noeud représentant le noeud n
     * @param  n noeud dont on cherche le représentant
     * @return   pointeur sur le noeud représentant
     */
    Node* Find(Node* n) ;

    /**
     * Fusionne les classes de 2 noeuds
     * @param n1
     * @param n2
     */
    void Union(Node* n1, Node* n2) ;

    /** Retourne vrai si toutes les classes ont été fusionnée */
    bool done() ;

    /** Retourne le pointeur du noeud asocié à un entier */
    Node* node(int n) ;

  private:
    // Ensemble des sets de départ
    std::vector<Node*> nodes ;

    // Indique si la classe à déjà été fusionnée
    std::vector<bool> classes_done ;
} ;

#endif //MMAZE_UNION_FIND_HPP
