#include "Plateau.hpp"

#include <cassert>

namespace MMaze {

Plateau::Plateau(int nb_tuiles) {
  Pioche pioche(nb_tuiles) ;

  // Génère une tuile de départ
  tuiles.push_back(Tuile(TUILE_DEPART)) ;
  graphe = tuiles[0].tuileToGraphe(0) ;
}

Plateau::~Plateau() {}

Tuile Plateau::getTuile(int index) {
  return tuiles[index] ;
}

void Plateau::passer_porte(TuileCase porte) {
  if (pioche.taille() == 0) {
    std::cerr << "Plus de tuiles disponibles." << std::endl ;
  }
  else {
    // récupère la case de la porte de sortie
    int porte_sortie = porte.index_case ;

    int index_tuile = tuiles.size() ;
    // Pioche une tuile aléatoirement, la tourne si besoin et l'insère
    Tuile t = pioche.piocher() ;
    t = t.tourne(rotation_porte_acces(porte_sortie)) ;
    tuiles.push_back(t) ;
    // Génère son graphe
    Graphe<TuileCase> g = t.tuileToGraphe(index_tuile) ;
    // le fusionne avec le graphe actuel
    graphe.fusion(g) ;
    // Modifie le graphe pour le passage de la porte
    int porte_ent = porte_entree(porte_sortie) ;
    passage_porte(graphe, porte, {index_tuile, porte_ent}) ;
  }
}

std::vector<TuileCase> Plateau::chemin(TuileCase start, TuileCase end) {
  sommet s_start = graphe.get_sommet(start) ;
  sommet s_end = graphe.get_sommet(end) ;
  std::vector<sommet> chemin_sommets = chemin_plus_court_a_star(graphe, s_start, s_end) ;
  return graphe.get_elements_chemin(chemin_sommets) ;
}

//affichage
std::ostream& operator<<(std::ostream& out, const Plateau& p) {
  for (Tuile t : p.tuiles) {
    out << t << std::endl ;
  }
  return out ;
}

/* Fonctions sur sommets propre au graphe de Tuile */

Direction direction_noeuds(int n1, int n2) {
  int res = n1 - n2 ;
  if (res < -3) {
    return BAS ;
  }
  else if (res > 3) {
    return HAUT ;
  }
  else if (res < 0) {
    return DROITE ;
  }
  else {
    return GAUCHE ;
  }
}

int longueur_noeuds(int n1, int n2) {
  switch(direction_noeuds(n1, n2)) {
    case GAUCHE  : return n2 - n1 ;
    case DROITE  : return n1 - n2 ;
    case HAUT    : return (n1 - n2) / 4 ;
    case BAS     : return (n2 - n1) / 4 ;
    default      : return -1 ;
  }
}

Direction direction_porte(int case_porte) {
  switch (case_porte) {
    case 2  : return BAS ;
    case 4  : return DROITE ;
    case 11 : return GAUCHE ;
    case 13 : return HAUT ;
    default : return HAUT ; // pour éviter warning... pas propre : solution ?
  }
}

void passage_porte(Graphe<TuileCase>& graphe, const TuileCase& porte_sortie, const TuileCase& porte_entree) {
  // Récupère les sommets du graphe associés aux 2 cases (start, end)
  sommet sortie = graphe.get_sommet(porte_sortie) ;
  sommet entree = graphe.get_sommet(porte_entree) ;
  assert (sortie != graphe.sommets()) ;
  assert (entree != graphe.sommets()) ;

  // Récupère les 2 directions à vérifier de part et d'autre de la porte
  Direction dir_sortie = direction_porte(porte_sortie.index_case) ;
  Direction dir_entree = direction_porte(porte_entree.index_case) ;

  // Ajoutes les arcs :

  graphe.add_arc(sortie, entree) ;

  // arcs sortie - tout la ligne accessible
  for (sommet s : graphe.voisins(entree)) {
    if (direction_noeuds(entree, s) == dir_entree) {
      graphe.add_arc(sortie, s) ;
    }
  }
  // arcs entrée - toute la ligne accessible
  for (sommet s : graphe.voisins(sortie)) {
    if (direction_noeuds(sortie, s) == dir_sortie) {
      graphe.add_arc(entree, s) ;
    }
  }

  // Le reste des cases sur la même ligne de part et d'autre de la porte
  // -> arcs sur le graphe
  for (sommet s : graphe.voisins(sortie)) {
    // Pour toutes les cases avec un accès droit à la sortie
    if (direction_noeuds(sortie, s) == dir_sortie) {
      // Pour toutes les cases de la tuile 2 avec un accès droit à l'entrée
      for(sommet s2 : graphe.voisins(entree)) {
        if (direction_noeuds(entree, s2) == dir_entree) {
          graphe.add_arc(s, s2) ;
        }
      }
    }
  }
  
}

}
