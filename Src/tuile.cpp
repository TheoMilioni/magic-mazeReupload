#include "tuile.hpp"
#include "couleurs.hpp"
#include "melangeur.hpp"
#include "UnionFind.hpp"
#include "Graphe.hpp"

#include <cassert>
#include <iostream>
#include <cstdlib>
#include <string>
#include <unistd.h>
#include <fstream>

namespace MMaze {

const bool DEBUG = false ;

Tuile::Tuile(TuileType type, Couleur couleur) {
  creer_murs() ;
  nb_sites = 0 ;

  if (type == TUILE_DEPART) {
    tuile_depart();
  } else {
    creer_portes() ;

    if (type == TUILE_OBJECTIF) {
      creer_site_unique(SITE_OBJECTIF, couleur) ;
    }
    else if (type == TUILE_SORTIE) {
      creer_site_unique(SITE_SORTIE, couleur) ;
    }
  }

  nettoyer_boutiques() ;
}

Tuile::~Tuile() {}

bool Tuile::mur(Mur m) const {
  int index = m.index() ;

  return murs[index] ;
}


bool Tuile::accessible(Case c) const {
  return !boutiques[c.index()] ;
}

bool Tuile::site(int c) const {
  bool found = false ;
  int i = 0 ;
  while (!found && i < nb_sites) {
    if (sites[i].getCase() == c) {
      found = true ;
    }
    i++ ;
  }
  return found ;
}

Site Tuile::getSite(int c) const {
  bool found = false ;
  int i = 0 ;
  while (!found && i < nb_sites) {
    if (sites[i].getCase() == c)  {
      found = true ;
      return sites[i] ;
    }
    i++ ;
  }
  return Site() ;
}

int Tuile::porte_aleatoire() {
  // Sélectionne les portes existantes
  std::vector<int> portes_dispo ;
  for (int i : {2, 4, 11, 13}) {
    if (site(i)) {
      if (getSite(i).getType() == SITE_PORTE) {
        portes_dispo.push_back(i) ;
      }
    }
  }

  int index_porte = rand_borne(0, portes_dispo.size() - 1) ;
  return portes_dispo[index_porte] ;
}

/** FONCTIONS AFFICHAGE *******************************************************/

void Tuile::afficher_horizontal(std::ostream& out, unsigned int i) const {
  assert(i < 5) ;
  if (i == 0) {
    out << "+---+---+" ;
    afficher_porte(out, 2) ;
    out << "+---+" ;
  }
  else if (i == 4) {
    out << "+---+" ;
    afficher_porte(out, 13) ;
    out << "+---+---+" ;
  } else {
    out << "+" ;
    for(unsigned int m = 0; m < 4; ++m) {
      Case up = Case(i-1, m) ;
      Case down = Case(i, m) ;
      if(mur(Mur(up, down))) {
        out << "---+" ;
      } else {
        out << "   +" ;
      }
    }
  }
}

void Tuile::afficher_vertical(std::ostream& out, unsigned int i) const {
  assert(i < 4) ;
  if (i == 1) {
    afficher_porte(out, 4) ;
  } else {
    out << "|" ;
  }
  for(unsigned int m = 0; m < 4; ++m) {
    //out << "   " ;
    afficher_case(out, m + 4 * i) ;

    if(m < 3) {
      Case left = Case(i, m) ;
      Case right = Case(i, m+1) ;
      if(m < 3 && mur(Mur(left, right))) {
        out << "|" ;
      } else {
        out << " " ;
      }
    }
  }
  if (i == 2) {
    afficher_porte(out, 11) ;
  } else {
    out << "|" ;
  }
}

void Tuile::afficher_porte(std::ostream& out, unsigned int pos) const {
  if (site(pos)) {
    const Site site = getSite(pos) ;
    if (site.getType() == SITE_PORTE) {
      out << txt_colors[site.getCouleur()] ;

      switch (pos) {
        case 2 : out << " ^ " ; break ;
        case 13 : out << " v " ;break ;
        case 4 : out << "<" ;break ;
        case 11 : out << ">" ;break ;
      }
      out << TXT_CLEAR ;
    }
  }
  else {
    if (pos == 2 || pos == 13) {
      out << "---" ;
    } else {
      out << "|" ;
    }
  }
}

void Tuile::afficher_case(std::ostream &out, unsigned int pos) const {
  if (site(pos)) {
    const Site site = getSite(pos) ;
    out << txt_colors[site.getCouleur()] ;
    switch (site.getType()) {
      case SITE_DEPART : out << " @ " ; break ;
      case SITE_OBJECTIF : out << " X " ; break ;
      case SITE_SORTIE : out << " ! " ; break ;
      default : out << "   " ; break ;
    }
    out << TXT_CLEAR ;
  }
  else {
    out << "   " ;
  }
}

std::ostream& operator<< (std::ostream& out, const Tuile& t) {
  for(unsigned int i = 0; i < 4; ++i) {
    t.afficher_horizontal(out, i) ;
    out << std::endl ;
    t.afficher_vertical(out, i) ;
    out << std::endl ;
  }
  t.afficher_horizontal(out, 4) ;
  return out ;
}

/** FONCTIONS POUR GENERATION DE TUILES ***************************************/

void Tuile::creer_portes() {
  int nb_portes = rand_borne(1, 4) ;

  creer_site(13, SITE_PORTE, AUCUNE) ;

  Melangeur cases(sizeof(int)) ;
  int tab_cases[3] = {4, 2, 11} ;
  cases.inserer(tab_cases, 3) ;

  Melangeur couleurs = creer_melangeur_couleurs() ;

  for (int i = 1; i < nb_portes; i++) {
    int pos ;
    Couleur couleur ;
    cases.retirer(&pos) ;
    couleurs.retirer(&couleur) ;

    creer_site(pos, SITE_PORTE, couleur) ;
  }
}

void Tuile::creer_site(int position, SiteType type, Couleur couleur) {
  Site site(position, type, couleur) ;
  //sites.insert(std::pair<int, Site>(position, site)) ;
  sites[nb_sites] = site ;
  nb_sites++ ;

  if (DEBUG) {
    std::cout
      << "Case " << position << ": " ;

      switch (type) {
        case SITE_PORTE: std::cout << "porte" ; break ;
        case SITE_DEPART: std::cout << "depart" ; break ;
        case SITE_OBJECTIF: std::cout << "objectif" ; break ;
        case SITE_SORTIE: std::cout << "sortie" ; break ;
        default: std::cout << "site " << type ; break ;
      }

    std::cout
      << ", "
      << "couleur "<< txt_colors[couleur]
      << "[" << couleur << "]"
      << TXT_CLEAR << "." << std::endl ;
  }
}

void Tuile::creer_murs() {
  // initialise tout les murs à vrai
  for (bool& mur : murs) {
    mur = true ;
  }

  UnionFind cases(16) ;

  Melangeur murs_random(sizeof(int)) ;
  for (int i = 0; i < MAX_MUR; i++) {
    murs_random.inserer(&i) ;
  }

  while (!cases.done()) {
    // Choix d'un mur aléatoire
    int mur ;
    murs_random.retirer(&mur) ;

    murs[mur] = false ;

    // Cases adjacentes
    Case case1 = Mur(mur)[0] ;
    Case case2 = Mur(mur)[1] ;

    // Noeud (UnionFind) correspondant aux classes
    Node* n1 = cases.node(case1.index()) ;
    Node* n2 = cases.node(case2.index()) ;

    cases.Union(n1, n2) ;
  }
}

void Tuile::creer_site_unique(SiteType type, Couleur couleur) {
  int tab_cases[12] = {0, 1, 3, 5, 6, 7, 8, 9, 10, 12, 14, 15} ;
  Melangeur cases(sizeof(int)) ;
  cases.inserer(tab_cases, 12) ;

  int position ;
  cases.retirer(&position) ;
  creer_site(position, type, couleur) ;
}

void Tuile::tuile_depart() {
  Melangeur couleurs = creer_melangeur_couleurs() ;
  for (int porte : {2, 4, 11, 13}) {
    Couleur couleur ;
    couleurs.retirer(&couleur) ;
    creer_site(porte, SITE_PORTE, couleur) ;
  }

  Melangeur couleurs_dep = creer_melangeur_couleurs() ;
  for (int depart : {5, 6, 9, 10}) {
    Couleur couleur ;
    couleurs_dep.retirer(&couleur) ;
    creer_site(depart, SITE_DEPART, couleur) ;
  }
}

/** GESTION BOUTIQUES *********************************************************/

bool Tuile::mur_direction(int index_case, Direction dir) {
  Case c(index_case) ;
  try {
    Case cVoisine = c.voisine(dir) ;
    Mur m(c, cVoisine) ;
    return mur(m) ;
  } catch (const std::domain_error& e) {
    return true ;
  }
}

bool Tuile::impasse(int index_case) {
  if (site(index_case)) {
    return false ;
  }

  bool mur_brise = false ;
  bool mur_trouve = false ;
  Direction dir ;

  for (Direction d : {HAUT, BAS, GAUCHE, DROITE}) {
    mur_trouve = mur_direction(index_case, d) ;
    if (!mur_trouve) {
      dir = d ;
    }

    if (!mur_brise && !mur_trouve) {
      mur_brise = true ;
    }
    else if (mur_brise && !mur_trouve) {
      return false ;
    }
  }

  boutiques[index_case] = true ;
  Case thisCase = Case(index_case) ;

  try {
    Case nextCase = thisCase.voisine(dir) ;

    /*
    // Si dernière, reconstruis le mur
    if (!impasse(nextCase.index(), impasses)) {
      Mur m(thisCase, nextCase) ;
      murs[m.index()] = true ;
    }*/

    // Reconstruis le mur
    Mur m(thisCase, nextCase) ;
    murs[m.index()] = true ;

  } catch (const std::domain_error& e) {
    // rien
  }

  return true ;
}

void Tuile::nettoyer_boutiques() {
  // "ptr" sur case de travail
  //int caseActuelle = 0 ;

  // tab de stockage des impasses init à faux
  for (bool& b : boutiques) {
    b = false ;
  }

  bool op_faite ;
  do {
    op_faite = false ;
    for (int i = 0; i < 16; i++) {
      if (!boutiques[i]) {
        if (impasse(i)) {
          op_faite = true ;
        }
      }
    }
  } while (op_faite) ;

  /*
  std::cout << "Impasses: " ;
  for (int i = 0; i < 16; i++) {
    if (impasses[i]) {
      std::cout << i << " " ;
    }
  }
  std::cout << std::endl ;
  */

}

Tuile Tuile::tourne(int rotation) const {
  Tuile t ;
  t.clear() ;

  // Tourne l'ensemble des murs
  for (int i = 0; i < MAX_MUR; i++) {
    Mur m(i) ;
    if (mur(m)) {
      Mur mT = m.tourne(rotation) ;
      t.murs[mT.index()] = true ;
    }
  }

  // Tourne l'ensemble des sites
  assert(t.nb_sites == 0) ;
  for (int i = 0; i < nb_sites; i++) {
    Site site = sites[i] ;
    // Récupère la nouvelle case après rotation et créé un nouveau site par
    // copie (+ update Case)
    Case newCase = Case(site.getCase()).tourne(rotation) ;
    Site newSite(site) ;
    newSite.setCase(newCase.index()) ;
    t.sites[i] = newSite ;
    t.nb_sites++ ;
  }

  return t ;
}

void Tuile::clear() {
  for (bool& mur : murs) {
    mur = false ;
  }
  nb_sites = 0 ;
}

// dessiner le graphe à partir d'une tuile

Graphe<TuileCase> Tuile::tuileToGraphe(int idTuile) {
	Graphe<TuileCase> g ;
	for(int i = 0; i < MAX_CASE; i++) {
		// on ajoute les 16 cases dans le graphe
		g.add_sommet({idTuile, i}) ;
	}
	for(sommet s = 0; s < MAX_CASE; s ++) {
		// on ajoute les arretes de droite
		if(s != 3 && s != 7 && s != 11 && s != 15) {
			// les cases qui ont des voisins de droites (toutes sauf 3 7 11 15)
			// on decoupe ensuite la tuile par ligne
			if (s > 11) {
				if (! murs[15 + (s%4)*4])
					g.add_arc(s, s + 1) ;
			}
			else if (s > 7) {
				if (! murs[14 + (s%4)*4])
					g.add_arc(s, s + 1) ;
			}
			else if (s > 3) {
				if (! murs[13 + (s%4)*4])
					g.add_arc(s, s + 1) ;
			}
			else {
				if (! murs[12 + (s%4)*4])
					g.add_arc(s, s + 1) ;
			}
		}
		// on  ajoute les arretes du bas
		if (s < MAX_CASE - 4) {
			// les cases qui ont des voisins en bas (toutes sauf 12 13 14 15)
			if (! murs[s])
				g.add_arc(s,s + 4) ;
		}
	}
	g.add_arc_long() ;
	return g ;
}


/** FONCTIONS LECTURE/ECRITURE FICHIERS ***************************************/

void write_tuile(const Tuile& tuile, const char* path) {
  std::ofstream file ;
  file.open(path) ;
  if (file.is_open()) {
    file << "tuile\n" ;
    for (int i = 0; i < 24; i++) {
      if (tuile.mur(Mur(i))) file << "mur " << i << std::endl ;
    }
    for (int i = 0; i < tuile.nb_sites; i++) {
      Site site = tuile.sites[i] ;
      file << "site "
           << site.getCase() << " "
           << site.getTypeStr() << " "
           << nom_couleur(site.getCouleur())
           << std::endl ;
    }
    file << "fin" << std::endl ;
  }
  file.close() ;
}

Tuile read_tuile(const char* path) {
  Tuile t ;
  t.clear() ;

  std::ifstream file ;
  file.open(path) ;
  if (file.is_open()) {
    std::string word ;
    while (file >> word) {
      if (word == "mur") {
        int index_mur ;
        file >> index_mur ;
      }
    }
  }

  return t ;
}

/** FONCTIONS ANNEXES *********************************************************/

Melangeur creer_melangeur_couleurs() {
  Melangeur couleurs(sizeof(Couleur)) ;
  Couleur tab_couleurs[4] = {JAUNE, VERT, ORANGE, VIOLET} ;
  couleurs.inserer(tab_couleurs, 4) ;

  return couleurs ;
}

int rand_borne(int min, int max) {
  return rand() % (max - min + 1) + min ;
}

int rotation_porte_acces(int porte_index) {
  switch (porte_index) {
    case 2 : return 0 ;
    case 4 : return 1 ;
    case 11 : return -1 ;
    case 13 : return 2 ;
    default : return 0 ;
  }
}

int porte_entree(int porte_sortie) {
  switch (porte_sortie) {
    case 2 : return 13 ;
    case 4 : return 11 ;
    case 11 : return 4 ;
    case 13 : return 2 ;
    default : return 0 ;
  }
}

} //end of namespace MMaze
