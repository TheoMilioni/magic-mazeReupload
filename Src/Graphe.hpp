#ifndef MMAZE_GRAPHE_HPP
#define MMAZE_GRAPHE_HPP

#include <vector>
#include "direction.hpp"

namespace MMaze {

// Sommet du graphe : entiers positifs de 0 à n
typedef unsigned int sommet ;

// Graphe non orienté
// Template T :
//  - Graphe<int>         + simple pour test
//  - Graphe<TuileCase>   pour le projet
//
//  Pas d'autre valeurs possibles de T pour pouvoir séparer .h et .cpp
template <typename T>
class Graphe {
  public:
    // Initialise un graphe vide
    Graphe() ;

    ~Graphe() ;

    // Retourne le nombre de sommets du graphe
    unsigned int sommets() const ;

    // Ajoute un sommet au graphe
    sommet add_sommet(T) ;

    // Retourne le n° de sommet associé à un élément
    sommet get_sommet(T element) const ;

    // retourne l'élément associé à un n° de sommet
    T get_element(sommet s) const ;

    // Ajoute un arc/arete au graphe
    // Graphe non oriente donc (a, b) ajoute (b, a)
    void add_arc(sommet s1, sommet s2) ;

    // Retourne l'ensemble des noeuds accessibles depuis s
    std::vector<sommet> voisins(sommet s) const ;

  	// ajoute les arcs de long > 1
  	void add_arc_long() ;
	  void arc_long_3(sommet s, int i) ;
	  void arc_long_2(sommet s, int i) ;

    // Fusionne le graĥe actuel avec le graphe g
    // Donne un seul obj Graphe mais avec 2 groupes de sommets distincts
    void fusion(Graphe<T>& graphe) ;

    // Retourne l'ensemble des éléments associès à un chemin de sommets
    std::vector<T> get_elements_chemin(std::vector<sommet> chemin) ;

    // Affiche les listes d'adjacences (desc du graphe pour débug)
    void print_liste_adj() ;

  private:
    // Nombre de sommets (0 à n)
    unsigned int m_sommets ;

    // Liste d'adjacence
    // Ex : noeuds atteints par 3 : arcs[3] -> vector<int>
    std::vector<std::vector<sommet>> arcs ;

    // Données stockées dans le graphe
    // data[i] = donnée associé au sommet i
    std::vector<T> data ;
};

/**
  ALGOS DE CHEMIN LE PLUS COURT : BFS et A*
 */

// Parcours en largeur (BFS breadth-first search)
// remplit dist : distances
//         pred : predecesseurs
template <typename T>
void bfs(const Graphe<T>& g, sommet start, std::vector<int>& dist, std::vector<sommet>& prec) ;

// Retourne le chemin entre 2 pts à parti des tableaux dist et prec
std::vector<sommet> chemin(const std::vector<sommet>& prec, sommet start, sommet end) ;

// Chemin le plus court dans un graphe avec l'algo BFS (cf. au dessus)
// chemin : vector des sommets parcourus de start à end (compris)
// vector vide si chemin impossible
template <typename T>
std::vector<sommet> chemin_plus_court_bfs(const Graphe<T>& g, sommet start, sommet end) ;

// Chemin le plus court dans un graphe avec l'algo A*
// même resultat que précedemment
template <typename T>
std::vector<sommet> chemin_plus_court_a_star(const Graphe<T>& g, sommet start, sommet end) ;

// heuristique pour l'algo A* (dispo pour T : int, TuileCase)
template <typename T>
int heuristique(const Graphe<T>& g, sommet start, sommet end) ;

}

#endif // MMAZE_GRAPHE_HPP
