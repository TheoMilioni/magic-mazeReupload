#include "site.hpp"

namespace MMaze {

Site::Site() :
  index_case(-1),
  type(SITE_DEFAULT),
  couleur(AUCUNE) {}

Site::Site(int _index_case, SiteType _type, Couleur _couleur) :
  index_case(_index_case),
  type(_type),
  couleur(_couleur) {}

Site::Site(const Site& site) :
  index_case(site.index_case),
  type(site.type),
  couleur(site.couleur) {}

int Site::getCase() const {
  return index_case ;
}

SiteType Site::getType() const {
  return type ;
}

Couleur Site::getCouleur() const {
  return couleur ;
}

const char* Site::getTypeStr() const {
  static const char* noms[5] = {
    "default", "porte", "objectif", "sortie", "depart"
  } ;
  return noms[(int) type] ;
}

void Site::setCase(int _index_case) {
  index_case = _index_case ;
}

}
