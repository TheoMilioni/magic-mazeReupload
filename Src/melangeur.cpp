#include "melangeur.hpp"

#include <cstdlib> // malloc/free
#include <cassert> // assert
#include <cstring> // memcpy
#include <ctime>   // srand

namespace MMaze {

Melangeur::Melangeur(int octets) {
  mOctets = octets ;
  mCapacite = 1 ;
  mTaille = 0 ;
  mData = (char*) malloc(octets) ;
}

Melangeur::~Melangeur() {
  free(mData) ;
}

void Melangeur::inserer(const void* elem) {
  if (mCapacite == mTaille) {
    agrandit_capacite() ;
  }

  memcpy(mData + mOctets * mTaille, elem, mOctets) ;
  mTaille++ ;
}

void Melangeur::inserer(const void *elems, size_t n) {
  for (size_t i = 0; i < n; i++) {
    inserer((char*) elems + mOctets * i) ;
  }
}

void Melangeur::retirer(void* elem) {
  if (mTaille == 0) {
    return ;
  }

  char* ptr = mData + mOctets * indice_random() ;
  memcpy(elem, ptr, mOctets) ;

  // Déplace le dernier élement pour boucher le trou
  memcpy(ptr, mData + mOctets * (mTaille - 1), mOctets) ;

  mTaille-- ;
}

void Melangeur::vider() {
  mTaille = 0 ; // Réécrit depuis début pour les insert
}

int Melangeur::taille() {
  return mTaille ;
}

void Melangeur::agrandit_capacite() {
  mCapacite *= 2 ;
  char* tmp = (char*) realloc(mData, mOctets * mCapacite) ;
  assert(tmp) ; // mémoire bien ré-allouée
  mData = tmp ;
}

int Melangeur::indice_random() {
  return rand() % mTaille ;
}

} //end of namespace MMaze
