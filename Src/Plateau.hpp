#ifndef MMAZE_PLATEAU_HPP
#define MMAZE_PLATEAU_HPP

#include <vector>
#include "tuile.hpp"
#include "Graphe.hpp"
#include "pioche.hpp"

namespace MMaze {

// Jeu : ensemble de tuiles définissant le plateau
class Plateau {
  public:
    // Init un plateau avec un certains nombre de tuiles potentielles
    // (par défaut 20)
    Plateau(int nb_tuiles = 20) ;

    ~Plateau() ;

    // Retourne la tuile d'indice index
    Tuile getTuile(int index) ;

    // Passe une porte (= connexion de 2 tuiles)
    // Tire aléatoirement une tuile dans la pioche et l'insère dans la plateau
    // après la porte fournie en param (rotation si nécessaire)
    // + fusionne le graphe actuel avec le graphe de la tuile
    void passer_porte(TuileCase porte) ;

    // Retourne le plus court chemin entre 2 cases sur le plateau
    // (utilise A* dans Graphe)
    std::vector<TuileCase> chemin(TuileCase start, TuileCase end) ;

    // affichage de l'ensemble des tuiles découvertes par index croissant
    friend std::ostream& operator<<(std::ostream& out, const Plateau& p) ;

  private:
    // Stockage des tuiles actuellement découverte (~= dispo sur le plateau)
    std::vector<Tuile> tuiles ;
    // Graphe de l'ensemble du plateau disponible
    Graphe<TuileCase> graphe ;
    // Réserve de tuiles à "découvrir"
    Pioche pioche ;
};

// Direction d'un arc sentre 2 case sur la tuile
Direction direction_noeuds(int n1, int n2) ;

// Longueur (en nb de cases) entre 2 case sur une même tuile
int longueur_noeuds(int n1, int n2) ;

// Retourne la direction à vérifier par rapport à la porte de sortie lors de
// la connection de 2 tuiles
// ex : toutes les cases qui accèdent à porte 4 (DROITE) peuvent directement
// traverser
//
// | X X X <  11   4 < X X X
Direction direction_porte(int index_porte) ;

// Permet le passage entre deux tuiles (graphe déjà fusionné)
// Rajoute les arcs entre les cases de la ligne droite de part et d'autre de
// la porte
void passage_porte(Graphe<TuileCase>& graphe, const TuileCase& porte_sortie, const TuileCase& porte_entree) ;

}

#endif
