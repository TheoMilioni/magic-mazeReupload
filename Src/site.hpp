#ifndef MMAZE_SITE_HPP
#define MMAZE_SITE_HPP

#include "couleurs.hpp"
#include "case.hpp"

namespace MMaze {

// Type d'un Site
enum SiteType {
  SITE_DEFAULT,
  SITE_PORTE,
  SITE_OBJECTIF,
  SITE_SORTIE,
  SITE_DEPART
};

/**
 * Site particulier sur une case
 */
class Site {
  public:
    Site() ;
    Site(int index_case, SiteType type, Couleur couleur) ;
    Site(const Site& site) ;

    int getCase() const ;
    Couleur getCouleur() const ;
    SiteType getType() const ;
    const char* getTypeStr() const ;

    void setCase(int index_case) ;

  private:
    int index_case ;
    SiteType type ;
    Couleur couleur ;
};

}

#endif // MMAZE_SITE_HPP
