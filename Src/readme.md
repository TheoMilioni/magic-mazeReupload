Projet Magic Maze (LIFAP6)
==========================

Code disponible sur : https://forge.univ-lyon1.fr/p1510585/magic-maze-etu

Groupe
------
PAQUET Léo
MILIONI Théo

Executables (tests)
-------------------
	./test_melangeur
	./test_tuiles
	./test_graphe

Commentaires
------------
- Pas de leak avec valgrind sur pc mais leak sur forge (CI) ?
- Il manque l'IA (partie 'Stratégie de base') -> pistes de dev :
	- stockage des objectifs/sorties dans une map<TuileCase, sommet> dans le plateau lors de l'insertion d'une tuile dans le graphe
	- fonction dans Tuile pour fournir la porte la plus proche pour une position donnée
	- stockage des positions actuelles des 4 pions de couleur
	- stockage des chemins en cours (ex : déplacement vers objectif) pour ne pas recalculer ?
	- pour infos pions (position et chemin en cours) : map<COULEUR, position> et map<COULEUR, vector<sommet>> ?
	- A chaque tour :
		if (objectif valide ET sortie connue) deplacement d'un cran sur chemin vers sortie
		else if (objectif connu) deplacement d'un cran sur chemin vers objectif
		else deplacement d'un cran sur chemin vers porte la plus proche

	- pour joueur : fonction qui transforme chemin (vector<sommet>) en vector<DIRECTION>
		(en fonction du sommet i et i+1)
		au tour i, le joueur de la direction D, choisi parmi les 4 pions de couleurs un de ceux dont le chemin_transforme[i] est de sa direction
